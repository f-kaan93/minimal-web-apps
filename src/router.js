import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      component: () => import("./views/About.vue")
    },
    {
      path: "/chrono",
      name: "chrono",
      component: () => import("./views/Chrono")
    },
    {
      path: "/speaker",
      name: "speaker",
      component: () => import("./views/Speaker")
    },
    {
      path: "/tally",
      name: "tally",
      component: () => import("./views/Tally")
    },
    {
      path: "/metronome",
      name: "metronome",
      component: () => import("./views/Metronome")
    }
  ]
});
