Main page app link component
No slots

Usage

```html
<app-link title="title" subtitle="subtitle" to="path">
</app-link>  
```