Wrapper for double tap and double click effects.
If user double taps, first single tap function is executed
and then the double tap function is executed. Dont use it 
together with @click

Usage

```html
<double-tap 
  @singletap="singleClickFunction"
  @doubletap="doubleClickFunction">

  <!-- child components -->

</double-tap>
```
