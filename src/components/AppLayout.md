Main app wrapper that with header that includes back(home) button
and optional help button

App content will be centered on the page

No props

If help slot is used, help button on the right will automatically
appear, clicking the button will reveal a modal with contents of 
the slot in it

Usage

```html
<app-layout>
  <!-- app content -->
  <template #help> <!-- optional -->
  <!-- help content -->
  </template>
</app-layout>
```
