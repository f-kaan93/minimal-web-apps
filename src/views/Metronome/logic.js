import Tone from "tone";
import upbeat from "./assets/upbeat.wav";
import downbeat from "./assets/downbeat.wav";

class Metronome {
  samples;
  seq;
  constructor(tempo, { top, bottom }) {
    this.samples = new Tone.Sampler({
      C4: upbeat,
      "C#4": downbeat
    }).toMaster();

    this._prepSeq(top, bottom);
    Tone.Transport.bpm.value = tempo;
  }
  onStart(callback) {
    Tone.Transport.on("start", callback);
  }
  onStop(callback) {
    Tone.Transport.on("stop", callback);
  }
  changeTempo(newTempo) {
    Tone.Transport.bpm.value = newTempo;
  }
  _prepSeq(top, bottom) {
    if (this.seq) {
      this.seq.stop();
      this.seq.dispose();
    }
    let notes = [];
    for (let i = 0; i < top; i++) {
      if (i == 0) {
        notes.push("C#4");
      } else {
        notes.push("C4");
      }
    }

    this.seq = new Tone.Sequence(
      (time, note) => {
        this.samples.triggerAttack(note, time);
      },
      notes,
      bottom + "n"
    );
    this.seq.start();
  }
  changeTimeSignature(top, bottom) {
    const status = Tone.Transport.status;
    if (status == "started") {
      Tone.Transport.stop();
    }
    this._prepSeq(top, bottom);
    if (status == "started") {
      Tone.Transport.start();
    }
  }
  start() {
    Tone.Transport.start();
  }
  stop() {
    Tone.Transport.stop();
  }
  toggle() {
    Tone.Transport.toggle();
  }
}

export default Metronome;
